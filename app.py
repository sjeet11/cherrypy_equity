import cherrypy
import redis
from jinja2 import Environment, FileSystemLoader
import os
from datetime import datetime

# Getting the path of the current directory
path = os.path.dirname(os.path.abspath(__file__))
env = Environment(loader=FileSystemLoader(path), trim_blocks=True)

redis_data = redis.Redis(host='localhost', port=6379)
lenght = redis_data.llen("name")
output = []
for x in range(lenght):
    data_name = redis_data.lindex('name', x)
    data_code = redis_data.lindex('code', x)
    data_open = redis_data.lindex('open', x)
    data_high = redis_data.lindex('high', x)
    data_low = redis_data.lindex('low', x)
    data_close = redis_data.lindex('close', x)
# Adding the data in dictionary format
    stocks = dict(name=(data_name.decode('ascii')).strip(),
                  code=int(data_code.decode('ascii')),
                  open=float(data_open.decode('ascii')),
                  high=float(data_high.decode('ascii')),
                  low=float(data_low.decode('ascii')),
                  close=float(data_close.decode('ascii')))
    output.append(stocks)

# Sorting the list
output.sort(key=lambda item: item.get('open'), reverse=True)
top_ten = [output[x] for x in range(10)]


class Index(object):
    @cherrypy.expose()
    def index(self):
        template = env.get_template('index.html')
        return template.render(data=top_ten, today_date=datetime.now().date())

    @cherrypy.expose()
    def search_details(self, name):
        for x in output:
            if str(name).upper() == x.get("name"):
                template = env.get_template('details.html')
                Stock_details = x
                return template.render(details=Stock_details)
        else:
            template = env.get_template('invalid_input.html')
            names = [item.get("name") for item in output]
            return template.render(names_data=names)


if __name__ == "__main__":
    cherrypy.quickstart(Index())
