Prerequisites:
python ==3.7
cherrypy==3.8.0
Jinja2==2.7.3
redis==2.4.13


app.py is to deploy cherrypy web application on localhost server.
index.html is the main page.
details.html is the search result page.
invalid_input.html is the page for incorrect search keyword, this page has all the stock entry data which can be accessed by a single click.
Redis databse is set to localhost server, please refer to save data to redis. If redis DB is on a differnet server then change host to IP address of the host server.



Process:
Run the app.py file and goto http://127.0.0.1:8080 to access the Top ten stock entries.
Ensure that port 8080 is free on the machine running this python file.
